package ro.msg.learning.shop.controllers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import ro.msg.learning.shop.entities.Location;
import ro.msg.learning.shop.pojos.entities.StockData;
import ro.msg.learning.shop.repositories.LocationRepository;
import ro.msg.learning.shop.utils.AccessTokenUtil;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StockExportControllerTest {
    @Autowired
    private TestRestTemplate testRestTemplate;
    @Autowired
    private LocationRepository locationRepository;
    private HttpEntity httpEntity;

    @Before
    public void setUp() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(
            new MediaType("text", "csv"))
        );
        httpHeaders.add("Authorization", "Bearer " + (new AccessTokenUtil(testRestTemplate)).requestCustomerAccess());
        httpHeaders.setContentType(new MediaType("text", "csv"));
        httpEntity = new HttpEntity<>(httpHeaders);
    }

    @Test
    public void getAllStocksWhenNoParams() {
        ResponseEntity<?> responseEntity = testRestTemplate.exchange("/stocks/all", HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<StockData>>() {});
        Assert.assertTrue(responseEntity.getStatusCode().is2xxSuccessful());
    }

    @Test
    public void getAllStocksByLocationWhenValidLocation() {
        List<Location> locations = locationRepository.findAll();
        Location foundLocation = null;
        if(!locations.isEmpty())
            foundLocation = locations.get(0);
        else Assert.fail("No locations found.");

        ResponseEntity<?> responseEntity = testRestTemplate.exchange("/stocks?location=" + foundLocation.getId(), HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<StockData>>() {});
        Assert.assertTrue(responseEntity.getStatusCode().is2xxSuccessful());
    }

    @Test
    public void getAllStocksByLocationWhenInvalidLocation() {
        ResponseEntity<?> responseEntity = testRestTemplate.exchange("/stocks?location=-1", HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<StockData>>() {});
        Assert.assertTrue(responseEntity.getStatusCode().is4xxClientError());
    }

    @Test
    public void getAllStocksByLocationWhenNoLocation() {
        ResponseEntity<?> responseEntity = testRestTemplate.exchange("/stocks", HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<StockData>>() {});
        Assert.assertTrue(responseEntity.getStatusCode().is4xxClientError());
    }

    @Test
    public void getAllStocksByLocationWhenLocationButNoValue() {
        ResponseEntity<?> responseEntity = testRestTemplate.exchange("/stocks?location=", HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<StockData>>() {});
        Assert.assertTrue(responseEntity.getStatusCode().is4xxClientError());
    }
}