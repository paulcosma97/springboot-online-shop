package ro.msg.learning.shop.utils;

import org.springframework.boot.test.web.client.TestRestTemplate;

public class AccessTokenUtil {

    private TestRestTemplate testRestTemplate;

    public AccessTokenUtil(TestRestTemplate testRestTemplate) {
        this.testRestTemplate = testRestTemplate;
    }

    public String requestAdminAccess() {
        return requestAccessFor(testRestTemplate, "john", "john");
    }

    public String requestCustomerAccess() {
        return requestAccessFor(testRestTemplate, "ionica", "ionica");
    }

    private String requestAccessFor(TestRestTemplate testRestTemplate, String user, String password) {
        return testRestTemplate.withBasicAuth("dev", "secret").postForObject("/oauth/token?grant_type=password&username=" + user + "&password=" + password, null, String.class).split("\"")[3];
    }
}
