package ro.msg.learning.shop.strategies;

import ro.msg.learning.shop.entities.Location;
import ro.msg.learning.shop.entities.Product;
import ro.msg.learning.shop.entities.Stock;
import ro.msg.learning.shop.exceptions.LocationForTransactionNotFoundException;
import ro.msg.learning.shop.pojos.entities.TransactionData;
import ro.msg.learning.shop.repositories.ProductRepository;
import ro.msg.learning.shop.repositories.StockRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FirstFoundStrategy implements LocationStrategy {
    private StockRepository stockRepository;
    private ProductRepository productRepository;

    public FirstFoundStrategy(StockRepository stockRepository, ProductRepository productRepository) {
        this.stockRepository = stockRepository;
        this.productRepository = productRepository;
    }

    /**
     * @throws LocationForTransactionNotFoundException RuntimeError thrown if there are no Locations that have the product in stock
     */
    @Override
    public Location getLocation(TransactionData transactionData) {
        Map<Location, Integer> locationProductCount = new HashMap<>();

        for (Integer productId : transactionData.getProductQuantity().keySet()) {
            Product product = productRepository.findOne(productId);
            int requiredQuantity = transactionData.getProductQuantity().get(productId);
            List<Stock> stocksFound = stockRepository.findAllByProductAndQuantityGreaterThanEqual(product, requiredQuantity);
            if (!stocksFound.isEmpty()) {
                Stock stock = stocksFound.get(0);
                if (locationProductCount.containsKey(stock.getLocation())) {
                    int oldValue = locationProductCount.get(stock.getLocation());
                    locationProductCount.replace(stock.getLocation(), oldValue + 1);
                } else {
                    locationProductCount.put(stock.getLocation(), 1);
                }
            }
        }

        return locationProductCount.entrySet().parallelStream().filter(entry -> entry.getValue() == transactionData.getProductQuantity().keySet().size()).findAny().orElseThrow(LocationForTransactionNotFoundException::new).getKey();
    }
}
