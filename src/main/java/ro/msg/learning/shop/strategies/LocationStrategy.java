package ro.msg.learning.shop.strategies;

import ro.msg.learning.shop.entities.Location;
import ro.msg.learning.shop.pojos.entities.TransactionData;

public interface LocationStrategy {
    Location getLocation(TransactionData transactionData);
}
