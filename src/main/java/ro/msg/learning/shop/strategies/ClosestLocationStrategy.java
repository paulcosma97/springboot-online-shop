package ro.msg.learning.shop.strategies;

import ro.msg.learning.shop.entities.Location;
import ro.msg.learning.shop.entities.Stock;
import ro.msg.learning.shop.exceptions.LocationNotFoundException;
import ro.msg.learning.shop.pojos.distance.DistanceCalculator;
import ro.msg.learning.shop.pojos.entities.TransactionData;
import ro.msg.learning.shop.repositories.LocationRepository;
import ro.msg.learning.shop.repositories.StockRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClosestLocationStrategy implements LocationStrategy {
    private StockRepository stockRepository;
    private LocationRepository locationRepository;
    private DistanceCalculator distanceCalculator;

    public ClosestLocationStrategy(LocationRepository locationRepository, StockRepository stockRepository, DistanceCalculator distanceCalculator) {
        this.stockRepository = stockRepository;
        this.locationRepository = locationRepository;
        this.distanceCalculator = distanceCalculator;
    }

    @Override
    public Location getLocation(TransactionData transactionData) {
        Location start = transactionData.getShippedTo();

        List<Location> locations = new ArrayList<>();
        for (Location location : locationRepository.findAll()) {
            if (locationHasRequiredProducts(location, transactionData)) {
                locations.add(location);
            }
        }

        if (locations.isEmpty()) {
            throw new LocationNotFoundException();
        }

        return getClosestLocation(distanceCalculator.calculate(start, locations));
    }

    private boolean locationHasRequiredProducts(Location location, TransactionData transactionData) {
        int counter = 0;
        List<Stock> stocks = stockRepository.findAllByLocation(location);
        for (Stock stock : stocks) {
            int actualProduct = stock.getProduct().getId();
            int actualQuantity = stock.getQuantity();
            for (Map.Entry<Integer, Integer> entry : transactionData.getProductQuantity().entrySet()) {
                int requiredProduct = entry.getKey();
                int requiredQuantity = entry.getValue();

                if (actualProduct == requiredProduct && actualQuantity >= requiredQuantity) {
                    counter++;
                }
            }
        }

        return counter == transactionData.getProductQuantity().keySet().size();
    }

    private Location getClosestLocation(Map<Location, Long> distance) {
        return distance.entrySet().parallelStream().min((m1, m2) -> (int) (m1.getValue() - m2.getValue())).orElseThrow(LocationNotFoundException::new).getKey();
    }
}
