package ro.msg.learning.shop.entities.embeddables;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
@NoArgsConstructor
public class Address {
    @Column(nullable = false)
    private String addressCountry;
    @Column(nullable = false)
    private String addressCity;
    @Column(nullable = false)
    private String addressCounty;
    @Column(nullable = false)
    private String addressStreet;
}
