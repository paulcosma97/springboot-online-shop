package ro.msg.learning.shop.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
public class Revenue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne
    private Location location;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    private long sum;

    public Revenue(Location location, long sum) {
        this.location = location;
        this.sum = sum;
        this.date = Calendar.getInstance().getTime();
    }
}
