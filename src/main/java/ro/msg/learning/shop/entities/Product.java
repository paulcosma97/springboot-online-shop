package ro.msg.learning.shop.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@ToString(exclude = {"transactionDetails", "stocks"})
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String description;
    private long price;
    private double weight;
    @ManyToOne
    private ProductCategory category;
    @ManyToOne
    private Supplier supplier;
    @JsonIgnore
    @OneToMany(mappedBy = "product")
    private List<Stock> stocks;
    @JsonIgnore
    @OneToMany(mappedBy = "product")
    private List<TransactionDetail> transactionDetails;

    public Product(String name, String description, long price, double weight, ProductCategory category, Supplier supplier) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.weight = weight;
        this.category = category;
        this.supplier = supplier;
    }
}
