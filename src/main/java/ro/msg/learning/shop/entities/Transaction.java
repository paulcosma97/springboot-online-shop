package ro.msg.learning.shop.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ro.msg.learning.shop.entities.embeddables.Address;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@ToString(exclude = "transactionDetails")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @OneToOne(fetch = FetchType.EAGER)
    private Location shippedFrom;
    @ManyToOne
    private Customer customer;
    @Embedded
    private Address address;
    @JsonIgnore
    @OneToMany(mappedBy = "transaction")
    private List<TransactionDetail> transactionDetails;

    public Transaction(Location shippedFrom, Customer customer, String addressCountry, String addressCity, String addressCounty, String addressStreet) {
        this.shippedFrom = shippedFrom;
        this.customer = customer;
        this.address = new Address();
        this.address.setAddressCountry(addressCountry);
        this.address.setAddressCity(addressCity);
        this.address.setAddressCounty(addressCounty);
        this.address.setAddressStreet(addressStreet);
    }
}
