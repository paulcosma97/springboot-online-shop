package ro.msg.learning.shop.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@ToString(exclude = {"stocks", "revenues"})
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String addressCountry;
    @Column(nullable = false)
    private String addressCity;
    @Column(nullable = false)
    private String addressCounty;
    @Column(nullable = false)
    private String addressStreet;
    @JsonIgnore
    @OneToMany(mappedBy = "location")
    private List<Stock> stocks;
    @JsonIgnore
    @OneToMany(mappedBy = "location")
    private List<Revenue> revenues;

    public Location(String name, String addressCountry, String addressCity, String addressCounty, String addressStreet) {
        this.name = name;
        this.addressCountry = addressCountry;
        this.addressCity = addressCity;
        this.addressCounty = addressCounty;
        this.addressStreet = addressStreet;
    }
}
