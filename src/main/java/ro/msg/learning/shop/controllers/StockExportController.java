package ro.msg.learning.shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ro.msg.learning.shop.pojos.entities.StockData;
import ro.msg.learning.shop.services.StockExportService;

import java.util.List;

@RestController
public class StockExportController {
    private StockExportService stockExportService;

    @Autowired
    public StockExportController(StockExportService stockExportService) {
        this.stockExportService = stockExportService;
    }

    @GetMapping(path = "/stocks", produces = "text/csv")
    public List<StockData> stockExportAllByLocationCsv(@RequestParam(name = "location") int locationId) {
        return stockExportService.stockDataListForLocation(locationId);
    }

    @GetMapping(path = "/stocks/all", produces = "text/csv")
    public List<StockData> stockExportAllCsv() {
        return stockExportService.findAllStockData();
    }
}
