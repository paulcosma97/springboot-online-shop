package ro.msg.learning.shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ro.msg.learning.shop.entities.Customer;
import ro.msg.learning.shop.services.EntityViewService;

import java.util.List;

@RestController
public class EntityViewController {
    private EntityViewService viewService;

    @Autowired
    public EntityViewController(EntityViewService viewService) {
        this.viewService = viewService;
    }

    @SuppressWarnings("unchecked")
    @GetMapping(path = "/view/{entityName}", produces = "application/json")
    public List<Customer> viewCustomers(@PathVariable String entityName) {
        return viewService.getList(entityName);
    }
}
