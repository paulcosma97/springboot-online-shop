package ro.msg.learning.shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ro.msg.learning.shop.entities.Transaction;
import ro.msg.learning.shop.pojos.entities.TransactionData;
import ro.msg.learning.shop.services.TransactionCreateService;

import java.security.Principal;

@RestController
public class TransactionCreateController {
    private TransactionCreateService transactionCreateService;

    @Autowired
    public TransactionCreateController(TransactionCreateService transactionCreateService) {
        this.transactionCreateService = transactionCreateService;
    }

    @PostMapping(path = "/transaction/create", produces = "application/json")
    public Transaction createTransaction(@RequestBody TransactionData transactionData, Principal principal) {
        return transactionCreateService.createTransaction(transactionData, principal.getName());
    }
}
