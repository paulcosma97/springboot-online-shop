package ro.msg.learning.shop.pojos.distance.google.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Row {
    private List<Element> elements;
}
