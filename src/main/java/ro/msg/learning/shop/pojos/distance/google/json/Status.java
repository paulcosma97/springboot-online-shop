package ro.msg.learning.shop.pojos.distance.google.json;

public enum Status {
    OK, INVALID_REQUEST, MAX_ELEMENTS_EXCEEDED, OVER_QUERY_LIMIT, REQUEST_DENIED, UNKNOWN_ERROR, NOT_FOUND
}
