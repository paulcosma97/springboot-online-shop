package ro.msg.learning.shop.pojos.distance;

import ro.msg.learning.shop.entities.Location;

import java.util.List;
import java.util.Map;

public interface DistanceCalculator {
    Map<Location, Long> calculate(Location start, List<Location> end);
}
