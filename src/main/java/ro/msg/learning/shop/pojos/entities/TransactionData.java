package ro.msg.learning.shop.pojos.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.msg.learning.shop.entities.Location;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionData {
    private Map<Integer, Integer> productQuantity;
    private String addressCountry;
    private String addressCity;
    private String addressCounty;
    private String addressStreet;

    public Location getShippedTo() {
        return new Location(null, addressCountry, addressCity, addressCounty, addressStreet);
    }
}
