package ro.msg.learning.shop.pojos.distance.google.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Element {
    private DistanceOrDuration distance;
    private DistanceOrDuration duration;
    private Status status;
}
