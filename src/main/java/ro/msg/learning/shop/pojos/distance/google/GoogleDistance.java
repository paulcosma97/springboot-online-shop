package ro.msg.learning.shop.pojos.distance.google;

import lombok.Data;
import org.springframework.web.client.RestTemplate;
import ro.msg.learning.shop.entities.Location;
import ro.msg.learning.shop.exceptions.GoogleDistanceStatusNotOkException;
import ro.msg.learning.shop.pojos.distance.DistanceCalculator;
import ro.msg.learning.shop.pojos.distance.google.json.GoogleDistanceJson;
import ro.msg.learning.shop.pojos.distance.google.json.Status;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class GoogleDistance implements DistanceCalculator {
    private String key;
    private String url;
    private RestTemplate restTemplate;

    public GoogleDistance(String key, String url, RestTemplate restTemplate) {
        this.key = key;
        this.url = url;
        this.restTemplate = restTemplate;
    }

    @Override
    public Map<Location, Long> calculate(Location start, List<Location> allEnds) {
        Map<Location, Long> out = new HashMap<>();
        Map<String, String> params = new HashMap<>();

        params.put("key", key);
        params.put("origin", start.getAddressCountry() + "," + start.getAddressCounty() + "," + start.getAddressCity() + "," + start.getAddressStreet());

        for (Location location : allEnds) {
            String destination = location.getAddressCountry() + "," + location.getAddressCounty() + "," + location.getAddressCity() + "," + location.getAddressStreet();
            params.put("destination", destination);

            long newDist = getDistance(params, location);
            out.put(location, newDist);
        }
        return out;
    }

    private long getDistance(Map<String, String> params, Location location) {
        GoogleDistanceJson distanceJson = restTemplate.getForObject(getUrlWithDestination(url, location), GoogleDistanceJson.class, params);
        Status status = distanceJson.getRows().get(0).getElements().get(0).getStatus();
        if (status != Status.OK) {
            throw new GoogleDistanceStatusNotOkException();
        }

        return distanceJson.getRows().get(0).getElements().get(0).getDistance().getValue();
    }

    private String getUrlWithDestination(String url, Location location) {
        return url + location.getAddressCountry() + "," + location.getAddressCounty() + "," + location.getAddressCity() + "," + location.getAddressStreet();
    }
}
