package ro.msg.learning.shop.pojos.distance.google.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoogleDistanceJson {
    @JsonProperty(value = "destination_addresses")
    private List<String> destinationAddresses;
    @JsonProperty(value = "origin_addresses")
    private List<String> originAddresses;
    private List<Row> rows;
    private Status status;
}
