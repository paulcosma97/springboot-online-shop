package ro.msg.learning.shop.pojos.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.msg.learning.shop.entities.Stock;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockData {
    private int id;
    private int productId;
    private int locationId;
    private int quantity;

    public static StockData fromStock(Stock stock) {
        return new StockData(stock.getId(), stock.getProduct().getId(), stock.getLocation().getId(), stock.getQuantity());
    }

    public static List<StockData> fromStockList(List<Stock> stocks) {
        List<StockData> out = new ArrayList<>();
        for (Stock stock : stocks) {
            out.add(fromStock(stock));
        }
        return out;
    }
}
