package ro.msg.learning.shop.pojos.distance.google.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DistanceOrDuration {
    private String text;
    private long value;
}
