package ro.msg.learning.shop.handlers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ro.msg.learning.shop.exceptions.CsvConversionException;
import ro.msg.learning.shop.exceptions.LocationForTransactionNotFoundException;
import ro.msg.learning.shop.exceptions.LocationNotFoundException;

@ControllerAdvice
public class ServiceResponseExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = LocationNotFoundException.class)
    protected ResponseEntity<Object> handleLocationNotFoundException(RuntimeException ex, WebRequest request) {
        String response = "Requested location was not found";
        if (request.getParameter("location") != null) {
            response += " for id: " + request.getParameter("location");
        }
        return handleExceptionInternal(ex, response, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = CsvConversionException.class)
    protected ResponseEntity<Object> handleCsvConversionException(RuntimeException ex, WebRequest request) {
        String response = "Could not find class for requested type";
        return handleExceptionInternal(ex, response, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = LocationForTransactionNotFoundException.class)
    protected ResponseEntity<Object> handleLocationForTransactionNotFoundException(RuntimeException ex, WebRequest request) {
        String response = "Could not find a location for requested transaction";
        return handleExceptionInternal(ex, response, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}
