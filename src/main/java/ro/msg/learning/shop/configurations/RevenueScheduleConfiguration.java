package ro.msg.learning.shop.configurations;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import ro.msg.learning.shop.entities.Location;
import ro.msg.learning.shop.entities.Revenue;
import ro.msg.learning.shop.entities.TransactionDetail;
import ro.msg.learning.shop.repositories.RevenueRepository;
import ro.msg.learning.shop.repositories.TransactionDetailRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@EnableScheduling
public class RevenueScheduleConfiguration {
    private RevenueRepository revenueRepository;
    private TransactionDetailRepository transactionDetailRepository;

    @Autowired
    public RevenueScheduleConfiguration(RevenueRepository revenueRepository, TransactionDetailRepository transactionDetailRepository) {
        this.revenueRepository = revenueRepository;
        this.transactionDetailRepository = transactionDetailRepository;
    }

    @Scheduled(cron = "0 0 23 * * *")
    public void calculateRevenue() {
        List<TransactionDetail> transactions = transactionDetailRepository.findAllByRevenueEvaluatedFalse();
        if (transactions.isEmpty()) {
            return;
        }

        Map<Location, Revenue> revenueMap = new HashMap<>();

        for (TransactionDetail transaction : transactions) {
            transaction.setRevenueEvaluated(true);
            long price = transaction.getProduct().getPrice() * transaction.getQuantity();
            Location location = transaction.getTransaction().getShippedFrom();

            Revenue revenue;
            if (revenueMap.containsKey(location)) {
                revenue = revenueMap.get(location);
            } else {
                revenue = new Revenue(location, 0);
                revenueMap.put(location, revenue);
            }

            revenue.setSum(revenue.getSum() + price);
        }

        revenueRepository.save(revenueMap.values());
        transactionDetailRepository.save(transactions);
    }
}
