package ro.msg.learning.shop.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.msg.learning.shop.converters.CsvHttpMessageConverter;

@Configuration
public class ConverterConfiguration {
    @Bean
    public CsvHttpMessageConverter csvHttpMessageConverter() {
        return new CsvHttpMessageConverter();
    }
}
