package ro.msg.learning.shop.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import ro.msg.learning.shop.entities.Role;
import ro.msg.learning.shop.entities.User;
import ro.msg.learning.shop.repositories.RoleRepository;
import ro.msg.learning.shop.repositories.UserRepository;

import java.util.stream.Collectors;

@Configuration
public class GlobalAuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Autowired
    public GlobalAuthenticationConfiguration(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService());
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return username -> {
            User usr = userRepository.findByUsername(username);
            String[] roles = roleRepository.findAllByUsersContaining(usr).parallelStream().map(Role::getName).collect(Collectors.toList()).toArray(new String[0]);
            return new org.springframework.security.core.userdetails.User(usr.getUsername(), usr.getPassword(), true, true, true, true, AuthorityUtils.createAuthorityList(roles));
        };
    }
}
