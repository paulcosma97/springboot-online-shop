package ro.msg.learning.shop.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.msg.learning.shop.exceptions.LocationStrategyNotFoundException;
import ro.msg.learning.shop.pojos.distance.DistanceCalculator;
import ro.msg.learning.shop.repositories.LocationRepository;
import ro.msg.learning.shop.repositories.ProductRepository;
import ro.msg.learning.shop.repositories.StockRepository;
import ro.msg.learning.shop.strategies.ClosestLocationStrategy;
import ro.msg.learning.shop.strategies.FirstFoundStrategy;
import ro.msg.learning.shop.strategies.LocationStrategy;

@Configuration
public class LocationStrategyConfiguration {
    @Value("${online-shop.location-strategy.type:first-found}")
    private String strategyType;
    private StockRepository stockRepository;
    private ProductRepository productRepository;
    private LocationRepository locationRepository;
    private DistanceCalculator distanceCalculator;

    @Autowired
    public LocationStrategyConfiguration(StockRepository stockRepository, ProductRepository productRepository, LocationRepository locationRepository, DistanceCalculator distanceCalculator) {
        this.stockRepository = stockRepository;
        this.productRepository = productRepository;
        this.locationRepository = locationRepository;
        this.distanceCalculator = distanceCalculator;
    }

    @Bean
    public LocationStrategy getLocationStrategy() {
        switch (strategyType) {
            case "first-found":
                return new FirstFoundStrategy(stockRepository, productRepository);
            case "closest-location":
                return new ClosestLocationStrategy(locationRepository, stockRepository, distanceCalculator);
            default:
                throw new LocationStrategyNotFoundException();
        }
    }

}
