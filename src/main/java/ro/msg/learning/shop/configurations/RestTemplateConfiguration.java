package ro.msg.learning.shop.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.net.InetSocketAddress;
import java.net.Proxy;

@Configuration
public class RestTemplateConfiguration {
    @Value("${online-shop.proxy.enabled:false}")
    private boolean isProxyEnabled;
    @Value("${online-shop.proxy.domain}")
    private String proxyDomain;
    @Value("${online-shop.proxy.port}")
    private int proxyPort;

    @Bean
    public RestTemplate getRestTemplate() {
        if (isProxyEnabled) {
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            requestFactory.setProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyDomain, proxyPort)));
            return new RestTemplate(requestFactory);
        } else {
            return new RestTemplate();
        }
    }
}
