package ro.msg.learning.shop.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import ro.msg.learning.shop.exceptions.GoogleDistanceInvalidApiException;
import ro.msg.learning.shop.exceptions.GoogleDistanceKeyOrUrlNotSetException;
import ro.msg.learning.shop.pojos.distance.DistanceCalculator;
import ro.msg.learning.shop.pojos.distance.google.GoogleDistance;

@Configuration
public class DistanceCalculatorConfiguration {
    private RestTemplate restTemplate;
    @Value("${online-shop.location-strategy.closest-location.api:google}")
    private String api;
    @Value("${online-shop.location-strategy.closest-location.google.key}")
    private String key;
    @Value("${online-shop.location-strategy.closest-location.google.url}")
    private String url;

    @Autowired
    public DistanceCalculatorConfiguration(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Bean
    public DistanceCalculator getDistanceCalculator() {
        if (api.equalsIgnoreCase("google")) {
            return getGoogleDistance();
        } else {
            throw new GoogleDistanceInvalidApiException();
        }
    }

    private GoogleDistance getGoogleDistance() {
        if (key == null || url == null) {
            throw new GoogleDistanceKeyOrUrlNotSetException();
        }

        return new GoogleDistance(key, url, restTemplate);
    }
}
