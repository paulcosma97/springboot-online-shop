package ro.msg.learning.shop.odata;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ro.msg.learning.shop.entities.Transaction;
import ro.msg.learning.shop.pojos.entities.TransactionData;
import ro.msg.learning.shop.repositories.CustomerRepository;
import ro.msg.learning.shop.repositories.ProductRepository;
import ro.msg.learning.shop.repositories.TransactionDetailRepository;
import ro.msg.learning.shop.repositories.TransactionRepository;
import ro.msg.learning.shop.services.TransactionCreateService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Data
public class Storage {
    private CustomerRepository customerRepository;
    private ProductRepository productRepository;
    private TransactionRepository transactionRepository;
    private TransactionDetailRepository transactionDetailRepository;
    private TransactionCreateService transactionCreateService;
    private Mapper mapper;

    @Autowired
    public Storage(TransactionCreateService transactionCreateService, CustomerRepository customerRepository, ProductRepository productRepository, TransactionRepository transactionRepository, TransactionDetailRepository transactionDetailRepository) {
        this.customerRepository = customerRepository;
        this.productRepository = productRepository;
        this.transactionRepository = transactionRepository;
        this.transactionDetailRepository = transactionDetailRepository;
        this.transactionCreateService = transactionCreateService;
        mapper = new Mapper();
    }

    public Map<String, Object> findCustomer(int id) {
        return mapper.map(customerRepository.findOne(id));
    }

    public Map<String, Object> findProduct(int id) {
        return mapper.map(productRepository.findOne(id));
    }

    public Map<String, Object> findTransaction(int id) {
        return mapper.map(transactionRepository.findOne(id));
    }

    public Map<String, Object> findTransactionDetail(int id) {
        return mapper.map(transactionDetailRepository.findOne(id));
    }

    public List<Map<String, Object>> findCustomerSet() {
        return customerRepository.findAll().parallelStream().map(mapper::map).collect(Collectors.toList());
    }

    public List<Map<String, Object>> findProductSet() {
        return productRepository.findAll().parallelStream().map(mapper::map).collect(Collectors.toList());
    }

    public List<Map<String, Object>> findTransactionSet() {
        return transactionRepository.findAll().parallelStream().map(mapper::map).collect(Collectors.toList());
    }

    public List<Map<String, Object>> findTransactionDetailSet() {
        return transactionDetailRepository.findAll().parallelStream().map(mapper::map).collect(Collectors.toList());
    }

    public Map<String, Object> findCustomerOfTransaction(int id) {
        return mapper.map(transactionRepository.findOne(id).getCustomer());
    }

    public Map<String, Object> findProductOfTransactionDetail(int id) {
        return mapper.map(transactionDetailRepository.findOne(id).getProduct());
    }

    public Map<String, Object> findTransactionOfTransactionDetail(int id) {
        return mapper.map(transactionDetailRepository.findOne(id).getTransaction());
    }

    public List<Map<String, Object>> findTransactionSetByCustomer(int id) {
        return transactionRepository.findAllByCustomer(customerRepository.findOne(id)).parallelStream().map(mapper::map).collect(Collectors.toList());
    }

    public List<Map<String, Object>> findTransactionDetailSetByTransaction(int id) {
        return transactionDetailRepository.findAllByTransaction(transactionRepository.findOne(id)).parallelStream().map(mapper::map).collect(Collectors.toList());
    }

    public List<Map<String, Object>> findTransactionDetailSetByProduct(int id) {
        return transactionDetailRepository.findAllByProduct(productRepository.findOne(id)).parallelStream().map(mapper::map).collect(Collectors.toList());
    }

    public Transaction saveTransaction(TransactionData transactionData) {
        return transactionCreateService.createTransaction(transactionData, SecurityContextHolder.getContext().getAuthentication().getName());
    }
}
