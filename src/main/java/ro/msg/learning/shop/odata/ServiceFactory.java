package ro.msg.learning.shop.odata;

import org.apache.olingo.odata2.api.ODataService;
import org.apache.olingo.odata2.api.ODataServiceFactory;
import org.apache.olingo.odata2.api.edm.provider.EdmProvider;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.apache.olingo.odata2.api.processor.ODataContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceFactory extends ODataServiceFactory {
    private Storage storage;
    private EdmProvider provider;

    @Autowired
    public ServiceFactory(Storage storage, EdmProvider provider) {
        this.storage = storage;
        this.provider = provider;
    }

    @Override
    public ODataService createService(ODataContext oDataContext) throws ODataException {
        return createODataSingleProcessorService(provider, new Processor(storage));
    }
}

