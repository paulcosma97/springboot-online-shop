package ro.msg.learning.shop.odata;

import org.apache.olingo.odata2.api.edm.FullQualifiedName;

public class EntityProviderNames {
    public static final String ENTITY_CUSTOMER_SET = "CustomerSet";
    public static final String ENTITY_TRANSACTION_SET = "TransactionSet";
    public static final String ENTITY_TRANSACTION_DETAIL_SET = "TransactionDetailSet";
    public static final String ENTITY_PRODUCT_SET = "ProductSet";

    public static final String ENTITY_CUSTOMER = "Customer";
    public static final String ENTITY_TRANSACTION = "Transaction";
    public static final String ENTITY_TRANSACTION_DETAIL = "TransactionDetail";
    public static final String ENTITY_PRODUCT = "Product";

    public static final String ROLE_TRANSACTION_CUSTOMER = "Transaction_Customer";
    public static final String ROLE_CUSTOMER_TRANSACTION_SET = "Customer_TransactionSet";
    public static final String ROLE_TRANSACTION_DETAIL_TRANSACTION = "TransactionDetail_Transaction";
    public static final String ROLE_TRANSACTION_TRANSACTION_DETAIL_SET = "Transaction_TransactionDetailSet";
    public static final String ROLE_TRANSACTION_DETAIL_PRODUCT = "TransactionDetail_Product";
    public static final String ROLE_PRODUCT_TRANSACTION_DETAIL_SET = "Product_TransactionDetailSet";

    public static final String NAMESPACE = "ro.msg.learning.shop.entities";
    public static final String ENTITY_CONTAINER = "container";
    public static final String ASSOCIATION_SET = "associationSet";

    public static final FullQualifiedName ENTITY_TYPE_CUSTOMER = new FullQualifiedName(NAMESPACE, ENTITY_CUSTOMER);
    public static final FullQualifiedName ENTITY_TYPE_TRANSACTION = new FullQualifiedName(NAMESPACE, ENTITY_TRANSACTION);
    public static final FullQualifiedName ENTITY_TYPE_TRANSACTION_DETAIL = new FullQualifiedName(NAMESPACE, ENTITY_TRANSACTION_DETAIL);
    public static final FullQualifiedName ENTITY_TYPE_PRODUCT = new FullQualifiedName(NAMESPACE, ENTITY_PRODUCT);

    public static final FullQualifiedName ASSOCIATION_TRANSACTION_CUSTOMER = new FullQualifiedName(NAMESPACE, ROLE_TRANSACTION_CUSTOMER + "_" + ROLE_CUSTOMER_TRANSACTION_SET);
    public static final FullQualifiedName ASSOCIATION_TRANSACTION_DETAIL_TRANSACTION = new FullQualifiedName(NAMESPACE, ROLE_TRANSACTION_DETAIL_TRANSACTION + "_" + ROLE_TRANSACTION_TRANSACTION_DETAIL_SET);
    public static final FullQualifiedName ASSOCIATION_TRANSACTION_DETAIL_PRODUCT = new FullQualifiedName(NAMESPACE, ROLE_TRANSACTION_DETAIL_PRODUCT + "_" + ROLE_PRODUCT_TRANSACTION_DETAIL_SET);

    private EntityProviderNames() {
        throw new IllegalStateException("Utility class");
    }
}
