package ro.msg.learning.shop.odata;

import lombok.NoArgsConstructor;
import ro.msg.learning.shop.entities.Customer;
import ro.msg.learning.shop.entities.Product;
import ro.msg.learning.shop.entities.Transaction;
import ro.msg.learning.shop.entities.TransactionDetail;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public class Mapper {
    public Map<String, Object> map(Customer customer) {
        Map<String, Object> out = new HashMap<>();
        out.put("id", customer.getId());
        out.put("firstName", customer.getFirstName());
        out.put("lastName", customer.getLastName());
        return out;
    }

    public Map<String, Object> map(Product product) {
        Map<String, Object> out = new HashMap<>();
        out.put("id", product.getId());
        out.put("name", product.getName());
        out.put("description", product.getDescription());
        out.put("price", product.getPrice());
        out.put("weight", product.getWeight());
        return out;
    }

    public Map<String, Object> map(Transaction transaction) {
        Map<String, Object> out = new HashMap<>();
        out.put("id", transaction.getId());
        out.put("addressCountry", transaction.getAddress().getAddressCountry());
        out.put("addressCounty", transaction.getAddress().getAddressCounty());
        out.put("addressCity", transaction.getAddress().getAddressCity());
        out.put("addressStreet", transaction.getAddress().getAddressStreet());
        return out;
    }

    public Map<String, Object> map(TransactionDetail transactionDetail) {
        Map<String, Object> out = new HashMap<>();
        out.put("id", transactionDetail.getId());
        out.put("quantity", transactionDetail.getQuantity());
        out.put("revenueEvaluated", transactionDetail.isRevenueEvaluated());
        return out;
    }
}
