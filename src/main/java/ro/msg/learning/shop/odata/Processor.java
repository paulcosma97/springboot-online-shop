package ro.msg.learning.shop.odata;

import com.google.gson.Gson;
import org.apache.olingo.odata2.api.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.edm.EdmException;
import org.apache.olingo.odata2.api.edm.EdmLiteralKind;
import org.apache.olingo.odata2.api.edm.EdmSimpleType;
import org.apache.olingo.odata2.api.ep.EntityProvider;
import org.apache.olingo.odata2.api.ep.EntityProviderWriteProperties;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.apache.olingo.odata2.api.exception.ODataNotFoundException;
import org.apache.olingo.odata2.api.processor.ODataResponse;
import org.apache.olingo.odata2.api.processor.ODataSingleProcessor;
import org.apache.olingo.odata2.api.uri.KeyPredicate;
import org.apache.olingo.odata2.api.uri.info.GetEntitySetUriInfo;
import org.apache.olingo.odata2.api.uri.info.GetEntityUriInfo;
import org.apache.olingo.odata2.api.uri.info.PostUriInfo;
import ro.msg.learning.shop.entities.Transaction;
import ro.msg.learning.shop.exceptions.IncorrectClassException;
import ro.msg.learning.shop.pojos.entities.TransactionData;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

import static ro.msg.learning.shop.odata.EntityProviderNames.*;

public class Processor extends ODataSingleProcessor {
    private Storage storage;

    public Processor(Storage storage) {
        this.storage = storage;
    }

    @Override
    public ODataResponse readEntity(GetEntityUriInfo uriInfo, String contentType) throws ODataException {
        if (uriInfo.getNavigationSegments().isEmpty()) {
            EdmEntitySet entitySet = uriInfo.getStartEntitySet();
            Map<String, Object> out;

            switch (entitySet.getName()) {
                case ENTITY_CUSTOMER_SET:
                    out = storage.findCustomer(getId(uriInfo.getKeyPredicates().get(0)));
                    break;
                case ENTITY_PRODUCT_SET:
                    out = storage.findProduct(getId(uriInfo.getKeyPredicates().get(0)));
                    break;
                case ENTITY_TRANSACTION_SET:
                    out = storage.findTransaction(getId(uriInfo.getKeyPredicates().get(0)));
                    break;
                case ENTITY_TRANSACTION_DETAIL_SET:
                    out = storage.findTransactionDetail(getId(uriInfo.getKeyPredicates().get(0)));
                    break;
                default:
                    throw new ODataException();
            }

            if (out != null) {
                EntityProviderWriteProperties.ODataEntityProviderPropertiesBuilder providerPropertiesBuilder = EntityProviderWriteProperties.serviceRoot(getContext().getPathInfo().getServiceRoot());
                return EntityProvider.writeEntry(contentType, entitySet, out, providerPropertiesBuilder.build());
            }
        } else if (uriInfo.getNavigationSegments().size() == 1) {
            EdmEntitySet startSet = uriInfo.getStartEntitySet();
            EdmEntitySet entitySet = uriInfo.getTargetEntitySet();

            Map<String, Object> out = null;

            switch (startSet.getName()) {
                case ENTITY_TRANSACTION_SET:
                    out = storage.findCustomerOfTransaction(getId(uriInfo.getKeyPredicates().get(0)));
                    break;
                case ENTITY_TRANSACTION_DETAIL_SET:
                    if (ENTITY_PRODUCT_SET.equals(entitySet.getName())) {
                        out = storage.findProductOfTransactionDetail(getId(uriInfo.getKeyPredicates().get(0)));
                    } else if (ENTITY_TRANSACTION_SET.equals(entitySet.getName())) {
                        out = storage.findTransactionOfTransactionDetail(getId(uriInfo.getKeyPredicates().get(0)));
                    }
                    break;
                default:
                    throw new ODataException();
            }
            if (out != null) {
                return EntityProvider.writeEntry(contentType, uriInfo.getTargetEntitySet(), out, EntityProviderWriteProperties.serviceRoot(getContext().getPathInfo().getServiceRoot()).build());
            }
        }
        throw new ODataNotFoundException(ODataNotFoundException.ENTITY);
    }

    @Override
    public ODataResponse readEntitySet(GetEntitySetUriInfo uriInfo, String contentType) throws ODataException {
        if (uriInfo.getNavigationSegments().isEmpty()) {
            EdmEntitySet entitySet = uriInfo.getStartEntitySet();

            switch (entitySet.getName()) {
                case ENTITY_CUSTOMER_SET:
                    return EntityProvider
                        .writeFeed(contentType, entitySet, storage.findCustomerSet(), EntityProviderWriteProperties
                            .serviceRoot(getContext().getPathInfo().getServiceRoot()).build());
                case ENTITY_PRODUCT_SET:
                    return EntityProvider
                        .writeFeed(contentType, entitySet, storage.findProductSet(), EntityProviderWriteProperties
                            .serviceRoot(getContext().getPathInfo().getServiceRoot()).build());
                case ENTITY_TRANSACTION_SET:
                    return EntityProvider
                        .writeFeed(contentType, entitySet, storage.findTransactionSet(), EntityProviderWriteProperties
                            .serviceRoot(getContext().getPathInfo().getServiceRoot()).build());
                case ENTITY_TRANSACTION_DETAIL_SET:
                    return EntityProvider
                        .writeFeed(contentType, entitySet, storage.findTransactionDetailSet(), EntityProviderWriteProperties
                            .serviceRoot(getContext().getPathInfo().getServiceRoot()).build());
                default:
                    throw new ODataNotFoundException(ODataNotFoundException.ENTITY);
            }
        } else if (uriInfo.getNavigationSegments().size() == 1) {
            EdmEntitySet startSet = uriInfo.getStartEntitySet();
            EdmEntitySet entitySet = uriInfo.getTargetEntitySet();
            List<Map<String, Object>> out = null;

            if (startSet.getName().equals(ENTITY_TRANSACTION_SET)) {
                if (ENTITY_CUSTOMER_SET.equals(entitySet.getName())) {
                    out = storage.findTransactionSetByCustomer(getId(uriInfo.getKeyPredicates().get(0)));
                }
            } else if (startSet.getName().equals(ENTITY_TRANSACTION_DETAIL_SET)) {
                if (ENTITY_TRANSACTION_SET.equals(entitySet.getName())) {
                    out = storage.findTransactionDetailSetByTransaction(getId(uriInfo.getKeyPredicates().get(0)));
                } else if (ENTITY_PRODUCT_SET.equals(entitySet.getName())) {
                    out = storage.findTransactionDetailSetByProduct(getId(uriInfo.getKeyPredicates().get(0)));
                }
            }

            if (out != null) {
                return EntityProvider.writeFeed(contentType, entitySet, out, EntityProviderWriteProperties
                    .serviceRoot(getContext().getPathInfo().getServiceRoot()).build());
            }
        }
        throw new ODataNotFoundException(ODataNotFoundException.ENTITY);
    }

    private int getId(KeyPredicate key) throws EdmException {
        return ((EdmSimpleType) key.getProperty().getType()).valueOfString(key.getLiteral(), EdmLiteralKind.DEFAULT, key.getProperty().getFacets(), Integer.class);
    }

    @Override
    public ODataResponse createEntity(PostUriInfo uriInfo, InputStream content, String requestContentType, String contentType) throws ODataException {
        String requestedClassName = String.format("%s.%s", uriInfo.getTargetType().getNamespace(), uriInfo.getTargetType().getName());

        if (Transaction.class != getClass(requestedClassName)) {
            throw new IncorrectClassException();
        }

        Transaction transaction = storage.saveTransaction(getTransactionData(content));

        EntityProviderWriteProperties.ODataEntityProviderPropertiesBuilder providerPropertiesBuilder = EntityProviderWriteProperties.serviceRoot(getContext().getPathInfo().getServiceRoot());
        return EntityProvider.writeEntry(contentType, uriInfo.getStartEntitySet(), storage.getMapper().map(transaction), providerPropertiesBuilder.build());
    }

    private Class getClass(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            Logger.getGlobal().severe(e.getMessage());
        }
        return null;
    }

    private TransactionData getTransactionData(InputStream content) {
        StringBuilder contentOut;
        Gson gson = new Gson();
        try (Scanner scanner = new Scanner(content)) {
            contentOut = new StringBuilder();
            while (scanner.hasNextLine()) {
                contentOut.append(scanner.nextLine()).append(System.lineSeparator());
            }
        }
        return gson.fromJson(contentOut.toString(), TransactionData.class);
    }
}
