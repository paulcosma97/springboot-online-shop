package ro.msg.learning.shop.odata;

import org.apache.olingo.odata2.api.edm.EdmMultiplicity;
import org.apache.olingo.odata2.api.edm.EdmSimpleTypeKind;
import org.apache.olingo.odata2.api.edm.FullQualifiedName;
import org.apache.olingo.odata2.api.edm.provider.*;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ro.msg.learning.shop.odata.EntityProviderNames.*;

@Component
public class Provider extends EdmProvider {
    @Override
    public List<Schema> getSchemas() throws ODataException {
        Schema schema = new Schema();
        schema.setEntityTypes(Arrays.asList(
            getEntityType(ENTITY_TYPE_CUSTOMER),
            getEntityType(ENTITY_TYPE_PRODUCT),
            getEntityType(ENTITY_TYPE_TRANSACTION),
            getEntityType(ENTITY_TYPE_TRANSACTION_DETAIL)
        ));
        schema.setAssociations(Arrays.asList(
            getAssociation(ASSOCIATION_TRANSACTION_CUSTOMER),
            getAssociation(ASSOCIATION_TRANSACTION_DETAIL_TRANSACTION),
            getAssociation(ASSOCIATION_TRANSACTION_DETAIL_PRODUCT)
        ));
        schema.setEntityContainers(Collections.singletonList(
            new EntityContainer()
                .setName(ENTITY_CONTAINER)
                .setDefaultEntityContainer(true)
                .setEntitySets(Arrays.asList(
                    getEntitySet(ENTITY_CONTAINER, ENTITY_CUSTOMER_SET),
                    getEntitySet(ENTITY_CONTAINER, ENTITY_PRODUCT_SET),
                    getEntitySet(ENTITY_CONTAINER, ENTITY_TRANSACTION_SET),
                    getEntitySet(ENTITY_CONTAINER, ENTITY_TRANSACTION_DETAIL_SET)
                ))
                .setAssociationSets(Arrays.asList(
                    getAssociationSet(ENTITY_CONTAINER, ENTITY_TYPE_TRANSACTION, ENTITY_CUSTOMER_SET, ROLE_TRANSACTION_CUSTOMER),
                    getAssociationSet(ENTITY_CONTAINER, ENTITY_TYPE_TRANSACTION_DETAIL, ENTITY_TRANSACTION_SET, ROLE_TRANSACTION_DETAIL_TRANSACTION),
                    getAssociationSet(ENTITY_CONTAINER, ENTITY_TYPE_TRANSACTION_DETAIL, ENTITY_PRODUCT_SET, ROLE_TRANSACTION_DETAIL_PRODUCT)

                ))
        ));
        schema.setNamespace(NAMESPACE);

        return Collections.singletonList(schema);
    }

    @Override
    public EntityType getEntityType(FullQualifiedName edmFQName) throws ODataException {
        if (!NAMESPACE.equals(edmFQName.getNamespace())) {
            return null;
        }

        if (ENTITY_TYPE_CUSTOMER.getName().equals(edmFQName.getName())) {
            List<Property> properties = Arrays.asList(
                new SimpleProperty().setName("id").setType(EdmSimpleTypeKind.Int32).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("firstName").setType(EdmSimpleTypeKind.String).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("lastName").setType(EdmSimpleTypeKind.String).setFacets(new Facets().setNullable(false))
            );

            List<PropertyRef> propertyRefs = Collections.singletonList(new PropertyRef().setName("id"));
            List<NavigationProperty> navigationProperties = new ArrayList<>();
            Key key = new Key().setKeys(propertyRefs);

            return new EntityType().setKey(key).setProperties(properties).setNavigationProperties(navigationProperties).setName(ENTITY_TYPE_CUSTOMER.getName());
        } else if (ENTITY_TYPE_PRODUCT.getName().equals(edmFQName.getName())) {
            List<Property> properties = Arrays.asList(
                new SimpleProperty().setName("id").setType(EdmSimpleTypeKind.Int32).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("name").setType(EdmSimpleTypeKind.String).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("description").setType(EdmSimpleTypeKind.String).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("price").setType(EdmSimpleTypeKind.Int64).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("weight").setType(EdmSimpleTypeKind.Double).setFacets(new Facets().setNullable(false))
            );

            List<PropertyRef> propertyRefs = Collections.singletonList(new PropertyRef().setName("id"));
            List<NavigationProperty> navigationProperties = new ArrayList<>();
            Key key = new Key().setKeys(propertyRefs);

            return new EntityType().setKey(key).setProperties(properties).setNavigationProperties(navigationProperties).setName(ENTITY_TYPE_PRODUCT.getName());
        } else if (ENTITY_TYPE_TRANSACTION.getName().equals(edmFQName.getName())) {
            List<Property> properties = Arrays.asList(
                new SimpleProperty().setName("id").setType(EdmSimpleTypeKind.Int32).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("addressCountry").setType(EdmSimpleTypeKind.String).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("addressCounty").setType(EdmSimpleTypeKind.String).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("addressCity").setType(EdmSimpleTypeKind.String).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("addressStreet").setType(EdmSimpleTypeKind.String).setFacets(new Facets().setNullable(false))
            );

            List<PropertyRef> propertyRefs = Collections.singletonList(new PropertyRef().setName("id"));
            List<NavigationProperty> navigationProperties = Collections.singletonList(
                new NavigationProperty().setName("Customer")
                    .setRelationship(ASSOCIATION_TRANSACTION_CUSTOMER).setFromRole(ROLE_TRANSACTION_CUSTOMER).setToRole(ROLE_CUSTOMER_TRANSACTION_SET)
            );
            Key key = new Key().setKeys(propertyRefs);

            return new EntityType().setKey(key).setProperties(properties).setNavigationProperties(navigationProperties).setName(ENTITY_TYPE_TRANSACTION.getName());
        } else if (ENTITY_TYPE_TRANSACTION_DETAIL.getName().equals(edmFQName.getName())) {
            List<Property> properties = Arrays.asList(
                new SimpleProperty().setName("id").setType(EdmSimpleTypeKind.Int32).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("quantity").setType(EdmSimpleTypeKind.Int32).setFacets(new Facets().setNullable(false)),
                new SimpleProperty().setName("revenueEvaluated").setType(EdmSimpleTypeKind.Boolean).setFacets(new Facets().setNullable(false))
            );

            List<PropertyRef> propertyRefs = Collections.singletonList(new PropertyRef().setName("id"));
            List<NavigationProperty> navigationProperties = Arrays.asList(
                new NavigationProperty().setName("Product")
                    .setRelationship(ASSOCIATION_TRANSACTION_DETAIL_PRODUCT).setFromRole(ROLE_TRANSACTION_DETAIL_PRODUCT).setToRole(ROLE_PRODUCT_TRANSACTION_DETAIL_SET),
                new NavigationProperty().setName("Transaction")
                    .setRelationship(ASSOCIATION_TRANSACTION_DETAIL_TRANSACTION).setFromRole(ROLE_TRANSACTION_DETAIL_TRANSACTION).setToRole(ROLE_TRANSACTION_TRANSACTION_DETAIL_SET)
            );
            Key key = new Key().setKeys(propertyRefs);

            return new EntityType().setKey(key).setProperties(properties).setNavigationProperties(navigationProperties).setName(ENTITY_TYPE_TRANSACTION_DETAIL.getName());
        }

        throw new ODataException();
    }

    @Override
    public EntityContainerInfo getEntityContainerInfo(String name) throws ODataException {
        if (name == null || ENTITY_CONTAINER.equals(name)) {
            return new EntityContainerInfo().setName(ENTITY_CONTAINER).setDefaultEntityContainer(true);
        }

        return null;
    }

    @Override
    public EntitySet getEntitySet(String entityContainer, String name) throws ODataException {
        if (ENTITY_CONTAINER.equals(entityContainer)) {
            if (ENTITY_CUSTOMER_SET.equals(name)) {
                return new EntitySet().setName(name).setEntityType(ENTITY_TYPE_CUSTOMER);
            } else if (ENTITY_PRODUCT_SET.equals(name)) {
                return new EntitySet().setName(name).setEntityType(ENTITY_TYPE_PRODUCT);
            } else if (ENTITY_TRANSACTION_SET.equals(name)) {
                return new EntitySet().setName(name).setEntityType(ENTITY_TYPE_TRANSACTION);
            } else if (ENTITY_TRANSACTION_DETAIL_SET.equals(name)) {
                return new EntitySet().setName(name).setEntityType(ENTITY_TYPE_TRANSACTION_DETAIL);
            }
        }
        return null;
    }

    @Override
    public Association getAssociation(FullQualifiedName edmFQName) throws ODataException {
        if (NAMESPACE.equals(edmFQName.getNamespace())) {
            if (edmFQName.getName().equals(ASSOCIATION_TRANSACTION_CUSTOMER.getName())) {

                return new Association().setName(ASSOCIATION_TRANSACTION_CUSTOMER.getName())
                    .setEnd1(new AssociationEnd().setType(ENTITY_TYPE_TRANSACTION).setRole(ROLE_TRANSACTION_CUSTOMER).setMultiplicity(EdmMultiplicity.MANY))
                    .setEnd2(new AssociationEnd().setType(ENTITY_TYPE_CUSTOMER).setRole(ROLE_CUSTOMER_TRANSACTION_SET).setMultiplicity(EdmMultiplicity.ONE));

            } else if (edmFQName.getName().equals(ASSOCIATION_TRANSACTION_DETAIL_PRODUCT.getName())) {

                return new Association().setName(ASSOCIATION_TRANSACTION_DETAIL_PRODUCT.getName())
                    .setEnd1(new AssociationEnd().setType(ENTITY_TYPE_TRANSACTION_DETAIL).setRole(ROLE_TRANSACTION_DETAIL_PRODUCT).setMultiplicity(EdmMultiplicity.MANY))
                    .setEnd2(new AssociationEnd().setType(ENTITY_TYPE_PRODUCT).setRole(ROLE_PRODUCT_TRANSACTION_DETAIL_SET).setMultiplicity(EdmMultiplicity.ONE));

            } else if (edmFQName.getName().equals(ASSOCIATION_TRANSACTION_DETAIL_TRANSACTION.getName())) {

                return new Association().setName(ASSOCIATION_TRANSACTION_DETAIL_TRANSACTION.getName())
                    .setEnd1(new AssociationEnd().setType(ENTITY_TYPE_TRANSACTION_DETAIL).setRole(ROLE_TRANSACTION_DETAIL_TRANSACTION).setMultiplicity(EdmMultiplicity.MANY))
                    .setEnd2(new AssociationEnd().setType(ENTITY_TYPE_TRANSACTION).setRole(ROLE_TRANSACTION_TRANSACTION_DETAIL_SET).setMultiplicity(EdmMultiplicity.ONE));
            }
        }
        return null;
    }

    @Override
    public AssociationSet getAssociationSet(String entityContainer, FullQualifiedName association, String sourceEntitySetName, String sourceEntitySetRole) throws ODataException {
        if (ENTITY_CONTAINER.equals(entityContainer)) {
            if (ASSOCIATION_TRANSACTION_CUSTOMER.equals(association)) {
                return new AssociationSet().setName(ASSOCIATION_SET)
                    .setAssociation(ASSOCIATION_TRANSACTION_CUSTOMER)
                    .setEnd1(new AssociationSetEnd().setRole(ROLE_CUSTOMER_TRANSACTION_SET).setEntitySet(ENTITY_CUSTOMER_SET))
                    .setEnd2(new AssociationSetEnd().setRole(ROLE_TRANSACTION_CUSTOMER).setEntitySet(ENTITY_TRANSACTION_SET));
            } else if (ASSOCIATION_TRANSACTION_DETAIL_PRODUCT.equals(association)) {
                return new AssociationSet().setName(ASSOCIATION_SET)
                    .setAssociation(ASSOCIATION_TRANSACTION_DETAIL_PRODUCT)
                    .setEnd1(new AssociationSetEnd().setRole(ROLE_PRODUCT_TRANSACTION_DETAIL_SET).setEntitySet(ENTITY_PRODUCT_SET))
                    .setEnd2(new AssociationSetEnd().setRole(ROLE_TRANSACTION_DETAIL_PRODUCT).setEntitySet(ENTITY_TRANSACTION_DETAIL_SET));
            } else if (ASSOCIATION_TRANSACTION_DETAIL_TRANSACTION.equals(association)) {
                return new AssociationSet().setName(ASSOCIATION_SET)
                    .setAssociation(ASSOCIATION_TRANSACTION_DETAIL_TRANSACTION)
                    .setEnd1(new AssociationSetEnd().setRole(ROLE_TRANSACTION_TRANSACTION_DETAIL_SET).setEntitySet(ENTITY_TRANSACTION_SET))
                    .setEnd2(new AssociationSetEnd().setRole(ROLE_TRANSACTION_DETAIL_TRANSACTION).setEntitySet(ENTITY_TRANSACTION_DETAIL_SET));
            }
        }
        return null;
    }
}
