package ro.msg.learning.shop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ro.msg.learning.shop.entities.Location;
import ro.msg.learning.shop.entities.Product;
import ro.msg.learning.shop.entities.Stock;

import java.util.List;
import java.util.Set;

public interface StockRepository extends JpaRepository<Stock, Integer> {
    List<Stock> findAllByProductAndLocation(Product product, Location location);

    List<Stock> findAllByLocation(Location location);

    List<Stock> findAllByProductAndQuantityGreaterThanEqual(Product product, int quantityGreaterThanEqualTo);

    @Query(value = "SELECT stock.location from Stock stock where stock.product in :products")
    List<Location> findAllLocationsByProductsInSet(Set<Product> products);
}
