package ro.msg.learning.shop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.msg.learning.shop.entities.Product;
import ro.msg.learning.shop.entities.Transaction;
import ro.msg.learning.shop.entities.TransactionDetail;

import java.util.List;

public interface TransactionDetailRepository extends JpaRepository<TransactionDetail, Integer> {
    List<TransactionDetail> findAllByTransaction(Transaction transaction);

    List<TransactionDetail> findAllByProduct(Product product);

    List<TransactionDetail> findAllByRevenueEvaluatedFalse();
}
