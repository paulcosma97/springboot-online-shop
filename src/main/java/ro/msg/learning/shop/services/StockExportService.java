package ro.msg.learning.shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.msg.learning.shop.entities.Location;
import ro.msg.learning.shop.entities.Stock;
import ro.msg.learning.shop.exceptions.LocationNotFoundException;
import ro.msg.learning.shop.pojos.entities.StockData;
import ro.msg.learning.shop.repositories.LocationRepository;
import ro.msg.learning.shop.repositories.StockRepository;

import java.util.List;

@Service
@Transactional
public class StockExportService {
    private StockRepository stockRepository;
    private LocationRepository locationRepository;

    @Autowired
    public StockExportService(StockRepository stockRepository, LocationRepository locationRepository) {
        this.stockRepository = stockRepository;
        this.locationRepository = locationRepository;
    }

    /**
     * @throws LocationNotFoundException RuntimeException thrown if there is no location found with the given id
     */
    public List<Stock> stocksForLocation(int locationId) {
        Location location = locationRepository.findOne(locationId);
        if (location == null) {
            throw new LocationNotFoundException();
        }
        return stockRepository.findAllByLocation(location);
    }

    public List<StockData> stockDataListForLocation(int locationId) {
        return StockData.fromStockList(stocksForLocation(locationId));
    }

    public List<StockData> findAllStockData() {
        return StockData.fromStockList(stockRepository.findAll());
    }
}
