package ro.msg.learning.shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.msg.learning.shop.repositories.*;

import java.util.Collections;
import java.util.List;

@Service
public class EntityViewService {
    private CustomerRepository customerRepository;
    private LocationRepository locationRepository;
    private ProductRepository productRepository;
    private ProductCategoryRepository productCategoryRepository;
    private RevenueRepository revenueRepository;
    private RoleRepository roleRepository;
    private StockRepository stockRepository;
    private SupplierRepository supplierRepository;
    private TransactionRepository transactionRepository;
    private TransactionDetailRepository transactionDetailRepository;
    private UserRepository userRepository;

    @Autowired
    public EntityViewService(CustomerRepository customerRepository, LocationRepository locationRepository, ProductRepository productRepository, ProductCategoryRepository productCategoryRepository, RevenueRepository revenueRepository, RoleRepository roleRepository, StockRepository stockRepository, SupplierRepository supplierRepository, TransactionRepository transactionRepository, TransactionDetailRepository transactionDetailRepository, UserRepository userRepository) {
        this.customerRepository = customerRepository;
        this.locationRepository = locationRepository;
        this.productRepository = productRepository;
        this.productCategoryRepository = productCategoryRepository;
        this.revenueRepository = revenueRepository;
        this.roleRepository = roleRepository;
        this.stockRepository = stockRepository;
        this.supplierRepository = supplierRepository;
        this.transactionRepository = transactionRepository;
        this.transactionDetailRepository = transactionDetailRepository;
        this.userRepository = userRepository;
    }

    public List getList(String entityName) {
        switch (entityName) {
            case "customers":
                return customerRepository.findAll();
            case "locations":
                return locationRepository.findAll();
            case "products":
                return productRepository.findAll();
            case "productCategories":
                return productCategoryRepository.findAll();
            case "revenues":
                return revenueRepository.findAll();
            case "roles":
                return roleRepository.findAll();
            case "stocks":
                return stockRepository.findAll();
            case "suppliers":
                return supplierRepository.findAll();
            case "transactions":
                return transactionRepository.findAll();
            case "transactionDetails":
                return transactionDetailRepository.findAll();
            case "users":
                return userRepository.findAll();
            default:
                return Collections.singletonList("Entity not found.");
        }
    }
}
