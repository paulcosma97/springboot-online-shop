package ro.msg.learning.shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.msg.learning.shop.entities.*;
import ro.msg.learning.shop.pojos.entities.TransactionData;
import ro.msg.learning.shop.repositories.*;
import ro.msg.learning.shop.strategies.LocationStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class TransactionCreateService {

    private StockRepository stockRepository;
    private CustomerRepository customerRepository;
    private TransactionDetailRepository transactionDetailRepository;
    private TransactionRepository transactionRepository;
    private ProductRepository productRepository;
    private LocationStrategy locationStrategy;

    @Autowired
    public TransactionCreateService(StockRepository stockRepository, CustomerRepository customerRepository, TransactionDetailRepository transactionDetailRepository, TransactionRepository transactionRepository, ProductRepository productRepository, LocationStrategy locationStrategy) {
        this.stockRepository = stockRepository;
        this.customerRepository = customerRepository;
        this.transactionDetailRepository = transactionDetailRepository;
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        this.locationStrategy = locationStrategy;
    }

    public Transaction createTransaction(TransactionData transactionData, String username) {
        Customer customer = customerRepository.findByUserUsername(username);
        Location shippedFrom = locationStrategy.getLocation(transactionData);
        Transaction transaction = new Transaction(shippedFrom, customer, transactionData.getAddressCountry(), transactionData.getAddressCity(), transactionData.getAddressCounty(), transactionData.getAddressCity());

        List<TransactionDetail> transactionDetails = new ArrayList<>();
        List<Stock> stocksChanged = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : transactionData.getProductQuantity().entrySet()) {
            Product product = productRepository.findOne(entry.getKey());
            transactionDetails.add(new TransactionDetail(transaction, product, entry.getValue()));

            Stock stockChanged = stockRepository.findAllByProductAndLocation(product, shippedFrom).get(0);
            stockChanged.setQuantity(stockChanged.getQuantity() - entry.getValue());
            stocksChanged.add(stockChanged);
        }

        transactionRepository.save(transaction);
        transactionDetailRepository.save(transactionDetails);
        stockRepository.save(stocksChanged);
        return transaction;
    }
}
