package ro.msg.learning.shop.converters;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractGenericHttpMessageConverter;
import ro.msg.learning.shop.exceptions.CsvConversionException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Logger;

public class CsvHttpMessageConverter extends AbstractGenericHttpMessageConverter<List> {

    public CsvHttpMessageConverter() {
        super(new MediaType("text", "csv"));
    }

    @Override
    protected void writeInternal(List objList, Type type, HttpOutputMessage outputMessage) throws IOException {
        Type actualType = ((ParameterizedType) type).getActualTypeArguments()[0];

        try {
            toCsv(Class.forName(actualType.getTypeName()), objList, outputMessage.getBody());
        } catch (ClassNotFoundException e) {
            Logger.getGlobal().severe(e.getMessage());
        }
    }

    @Override
    protected List readInternal(Class clazz, HttpInputMessage inputMessage) throws IOException {
        return fromCsv(clazz, inputMessage.getBody());
    }

    @Override
    public List read(Type type, Class contextClass, HttpInputMessage inputMessage) throws IOException {
        Type actualType = ((ParameterizedType) type).getActualTypeArguments()[0];
        try {
            return fromCsv(Class.forName(actualType.getTypeName()), inputMessage.getBody());
        } catch (ClassNotFoundException e) {
            Logger.getGlobal().severe(e.getMessage());
        }
        throw new CsvConversionException();
    }

    private List fromCsv(Class clazz, InputStream inputStream) throws IOException {
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema schema = csvMapper
            .schemaFor(clazz)
            .withHeader();
        MappingIterator mappingIterator = csvMapper.readerFor(clazz).with(schema).readValues(inputStream);
        return mappingIterator.readAll();
    }

    private void toCsv(Class clazz, List objList, OutputStream outputStream) throws IOException {
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema schema = csvMapper
            .schemaFor(clazz)
            .withHeader()
            .withColumnReordering(false);
        ObjectWriter objectWriter = csvMapper.writerFor(clazz).with(schema);
        objectWriter.writeValuesAsArray(outputStream).writeAll(objList);
        outputStream.close();
    }

    @Override
    public boolean canRead(Type type, Class<?> contextClass, MediaType mediaType) {
        boolean isList = false;
        if (type instanceof ParameterizedType) {
            isList = ((ParameterizedType) type).getRawType().getTypeName().equalsIgnoreCase(List.class.getName());
        }

        return isList && super.canWrite(type, contextClass, mediaType);
    }

    @Override
    public boolean canWrite(Type type, Class<?> clazz, MediaType mediaType) {
        boolean isList = false;
        if (type instanceof ParameterizedType) {
            isList = ((ParameterizedType) type).getRawType().getTypeName().equalsIgnoreCase(List.class.getName());
        }

        return isList && super.canWrite(type, clazz, mediaType);
    }
}
