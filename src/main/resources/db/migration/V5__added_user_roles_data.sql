INSERT INTO ROLE(name) VALUES
('USER'),
('ADMIN');

INSERT INTO USER(username, password) VALUES
('john', 'john'),
('ionica', 'ionica');

INSERT INTO USER_ROLES VALUES
(1, 1),
(1, 2),
(2, 1);