ALTER TABLE CUSTOMER ADD USER_ID INT NULL;
ALTER TABLE CUSTOMER
ADD CONSTRAINT CUSTOMER_USER_ID_fk
FOREIGN KEY (USER_ID) REFERENCES USER (ID);
ALTER TABLE CUSTOMER DROP USERNAME;